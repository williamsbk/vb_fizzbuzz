﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FizzBuzz
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    Try
      If disposing AndAlso components IsNot Nothing Then
        components.Dispose()
      End If
    Finally
      MyBase.Dispose(disposing)
    End Try
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.btnStart = New System.Windows.Forms.Button()
    Me.rtbOutput = New System.Windows.Forms.RichTextBox()
    Me.rbnUseList = New System.Windows.Forms.RadioButton()
    Me.rbnUseArray = New System.Windows.Forms.RadioButton()
    Me.rbnUseFor = New System.Windows.Forms.RadioButton()
    Me.SuspendLayout()
    '
    'btnStart
    '
    Me.btnStart.Location = New System.Drawing.Point(13, 13)
    Me.btnStart.Name = "btnStart"
    Me.btnStart.Size = New System.Drawing.Size(91, 41)
    Me.btnStart.TabIndex = 0
    Me.btnStart.Text = "Start"
    Me.btnStart.UseVisualStyleBackColor = True
    '
    'rtbOutput
    '
    Me.rtbOutput.Location = New System.Drawing.Point(13, 153)
    Me.rtbOutput.Name = "rtbOutput"
    Me.rtbOutput.ReadOnly = True
    Me.rtbOutput.Size = New System.Drawing.Size(331, 767)
    Me.rtbOutput.TabIndex = 1
    Me.rtbOutput.Text = ""
    '
    'rbnUseList
    '
    Me.rbnUseList.AutoSize = True
    Me.rbnUseList.Location = New System.Drawing.Point(13, 61)
    Me.rbnUseList.Name = "rbnUseList"
    Me.rbnUseList.Size = New System.Drawing.Size(150, 24)
    Me.rbnUseList.TabIndex = 2
    Me.rbnUseList.TabStop = True
    Me.rbnUseList.Text = "Use List Method"
    Me.rbnUseList.UseVisualStyleBackColor = True
    '
    'rbnUseArray
    '
    Me.rbnUseArray.AutoSize = True
    Me.rbnUseArray.Location = New System.Drawing.Point(13, 92)
    Me.rbnUseArray.Name = "rbnUseArray"
    Me.rbnUseArray.Size = New System.Drawing.Size(162, 24)
    Me.rbnUseArray.TabIndex = 3
    Me.rbnUseArray.TabStop = True
    Me.rbnUseArray.Text = "Use Array Method"
    Me.rbnUseArray.UseVisualStyleBackColor = True
    '
    'rbnUseFor
    '
    Me.rbnUseFor.AutoSize = True
    Me.rbnUseFor.Location = New System.Drawing.Point(13, 123)
    Me.rbnUseFor.Name = "rbnUseFor"
    Me.rbnUseFor.Size = New System.Drawing.Size(149, 24)
    Me.rbnUseFor.TabIndex = 4
    Me.rbnUseFor.TabStop = True
    Me.rbnUseFor.Text = "Use For Method"
    Me.rbnUseFor.UseVisualStyleBackColor = True
    '
    'FizzBuzz
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(356, 921)
    Me.Controls.Add(Me.rbnUseFor)
    Me.Controls.Add(Me.rbnUseArray)
    Me.Controls.Add(Me.rbnUseList)
    Me.Controls.Add(Me.rtbOutput)
    Me.Controls.Add(Me.btnStart)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "FizzBuzz"
    Me.Text = "FizzBuzz"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents btnStart As System.Windows.Forms.Button
  Friend WithEvents rtbOutput As System.Windows.Forms.RichTextBox
  Friend WithEvents rbnUseList As System.Windows.Forms.RadioButton
  Friend WithEvents rbnUseArray As System.Windows.Forms.RadioButton
  Friend WithEvents rbnUseFor As System.Windows.Forms.RadioButton

End Class
