﻿Public Class FizzBuzz

  Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click
    'Clears the Textbox
    If rtbOutput.TextLength > 0 Then
      rtbOutput.Clear()
    End If

    If rbnUseList.Checked = True Then
      'Calculate FizzBuzz
      ' x % 3 == 0 is Fizz
      ' x % 5 == 0 is Buzz
      ' if both then FizzBuzz
      Dim values As New List(Of Dictionary(Of Integer, String))
      Dim three As Boolean
      Dim five As Boolean
      For i As Integer = 1 To 100
        If i Mod 3 = 0 Then
          three = True
        End If
        If i Mod 5 = 0 Then
          five = True
        End If

        If three And five Then
          values.Add(New Dictionary(Of Integer, String)() From {{i, "FizzBuzz"}})
        ElseIf three Then
          values.Add(New Dictionary(Of Integer, String)() From {{i, "Fizz"}})
        ElseIf five Then
          values.Add(New Dictionary(Of Integer, String)() From {{i, "Buzz"}})
        Else
          values.Add(New Dictionary(Of Integer, String)() From {{i, i.ToString()}})
        End If
        three = False
        five = False
      Next

      'Write the Values to the text box
      For Each value As Dictionary(Of Integer, String) In values
        rtbOutput.AppendText(value.Values.First() + vbCrLf)
      Next value
    End If 'End rbnUseList

    If rbnUseArray.Checked = True Then
      rtbOutput.AppendText("Not Enabled Yet. :(")
    End If
    If rbnUseFor.Checked = True Then
      rtbOutput.AppendText("Not Enabled Yet. :(")
    End If

  End Sub
End Class
